from django.contrib import admin
from .models import *

class Contacts(admin.ModelAdmin):
    list_display = [field.name for field in Contact._meta.fields]

    fields = ["email"]

    class Meta:
        model = Contact

admin.site.register(Contact, Contacts)